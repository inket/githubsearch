//
//  GitHubRepository.swift
//  GitHubSearch
//

import Foundation

struct GitHubRepository: Codable {
    var fullName: String
    var description: String?
    var url: URL

    enum CodingKeys: String, CodingKey {
        case fullName = "full_name"
        case description
        case url = "html_url"
    }
}
