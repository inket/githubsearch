//
//  GitHubRepositorySearch.swift
//  GitHubRepositorySearch
//

import Foundation

private let resultsPerPage = 100

private struct ResultWrapper: Codable {
    let items: [GitHubRepository]
}

class GitHubRepositorySearch {
    let query: String
    var results: [GitHubRepository]?
    var isLoading: Bool = false

    init(query: String) {
        self.query = query
    }

    func loadResults(completion: @escaping () -> Void) {
        guard results == nil, !isLoading else { return }

        isLoading = true

        let request = GitHubAPIRequest(
            type: .searchRepositories,
            query: [
                "q": query,
                "page": String(1), // Implement pagination some other day
                "per_page": String(resultsPerPage)
            ]
        )

        print("Requesting \(request.url)")

        GitHubAPI.performRequest(request) { [weak self] response in
            guard let strongSelf = self else { return }

            switch response {
            case .success(data: let data):
                guard let decodedResult = try? JSONDecoder().decode(ResultWrapper.self, from: data) else {
                    fatalError("Error decoding results…")
                }

                if var results = strongSelf.results {
                    results.append(contentsOf: decodedResult.items)
                } else {
                    strongSelf.results = decodedResult.items
                }
            case .failure(error: let error):
                print("received error: \(error)")
            }

            strongSelf.isLoading = false
            DispatchQueue.main.async(execute: completion)
        }
    }
}
