//
//  GitHubAPI.swift
//  GitHubSearch
//

import UIKit

struct GitHubAPIRequest {
    enum RequestType {
        case searchRepositories
    }

    let type: RequestType
    var query: [String: String]?

    var url: URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.github.com"
        components.path = endpoint
        components.queryItems = query?.map(URLQueryItem.init(name:value:)) ?? []
        return components.url!
    }

    var endpoint: String {
        switch type {
        case .searchRepositories:
            return "/search/repositories"
        }
    }

    var method: String {
        switch type {
        case .searchRepositories:
            return "GET"
        }
    }

    var urlRequest: URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method
        return request
    }

    init(type: RequestType, query: [String: String]? = nil) {
        self.type = type
        self.query = query
    }
}

enum GitHubAPIResponse {
    enum APIError {
        case general(Error?)
        case rateLimited
        case validationFailed
    }

    case success(data: Data)
    case failure(error: APIError)
}

final class GitHubAPI {
    static var rateLimit: Int?
    static var rateLimitRemaining: Int?
    static var rateLimitReset: TimeInterval? {
        didSet {
            rateLimitResetTimer?.invalidate()

            guard let rateLimitReset = rateLimitReset else { return }

            DispatchQueue.main.async {
                let timeInterval: TimeInterval = rateLimitReset - Date().timeIntervalSince1970
                rateLimitResetTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { _ in
                    rateLimit = nil
                    rateLimitRemaining = nil
                    rateLimitUpdateCallback?(nil, nil)
                }
            }
        }
    }
    static var rateLimitResetTimer: Timer?
    static var rateLimitUpdateCallback: ((Int?, Int?) -> Void)?

    static var activityCount: Int = 0 {
        didSet {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = activityCount != 0
            }
        }
    }

    private init() {}

    static private func readRateLimitValues(from response: URLResponse) {
        guard let headers = (response as? HTTPURLResponse)?.allHeaderFields else { return }

        rateLimit = Int(headers["X-RateLimit-Limit"] as? String ?? "") ?? rateLimit
        rateLimitRemaining = Int(headers["X-RateLimit-Remaining"] as? String ?? "") ?? rateLimitRemaining
        rateLimitReset = TimeInterval(headers["X-RateLimit-Reset"] as? String ?? "") ?? rateLimitReset

        rateLimitUpdateCallback?(rateLimit, rateLimitRemaining)
    }

    static func performRequest(_ request: GitHubAPIRequest, callback: @escaping (GitHubAPIResponse) -> Void) {
        if let rateLimitRemaining = rateLimitRemaining, rateLimitRemaining == 0 {
            callback(.failure(error: .rateLimited))
            return
        }

        let dataTask = URLSession.shared.dataTask(with: request.urlRequest) { data, response, error in
            activityCount -= 1

            if let response = response {
                readRateLimitValues(from: response)
            }

            guard let httpResponse = response as? HTTPURLResponse else { fatalError("Unexpected case") }

            switch httpResponse.statusCode {
            case 200...299:
                callback(.success(data: data!))
            case 400...499:
                if httpResponse.statusCode == 403, rateLimitRemaining == 0 {
                    callback(.failure(error: .rateLimited))
                } else if httpResponse.statusCode == 422 {
                    callback(.failure(error: .validationFailed))
                } else {
                    fallthrough
                }
            default:
                callback(.failure(error: .general(error)))
            }
        }

        activityCount += 1
        dataTask.resume()
    }
}
