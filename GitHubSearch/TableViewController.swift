//
//  TableViewController.swift
//  GitHubSearch
//

import UIKit

class TableViewController: UITableViewController {
    var searches = [String: GitHubRepositorySearch]()
    var currentSearch: GitHubRepositorySearch?
    let searchController = UISearchController(searchResultsController: nil)
    var startSearchTimer: Timer?
    lazy var sectionHeaderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize, weight: .bold)
        label.textAlignment = .center
        label.backgroundColor = .white
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "GitHub Search"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self

        GitHubAPI.rateLimitUpdateCallback = { [weak self] _, remaining in
            DispatchQueue.main.async {
                switch remaining {
                case .none:
                    self?.sectionHeaderLabel.text = nil
                case let .some(count):
                    if count == 0 {
                        self?.sectionHeaderLabel.text = "Rate limit reached"
                    } else {
                        self?.sectionHeaderLabel.text = "\(count) requests remaining"
                    }
                }

                self?.loadResults()
            }
        }
    }

    func loadResults() {
        currentSearch?.loadResults { [weak self] in self?.tableView.reloadData() }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currentSearch = currentSearch else {
            // Predefined message
            return 1
        }

        return currentSearch.results?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)

        guard let currentSearch = currentSearch else {
            cell.textLabel?.text = nil
            cell.detailTextLabel?.text = "Start your search to find repositories on GitHub."
            return cell
        }

        guard let repository = currentSearch.results?[indexPath.row] else { return cell }
        cell.textLabel?.text = repository.fullName
        cell.detailTextLabel?.text = repository.description

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false

        guard let repository = currentSearch?.results?[indexPath.row] else { return }

        UIApplication.shared.open(repository.url, options: [:], completionHandler: nil)
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sectionHeaderLabel
    }
}

extension TableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let newQuery = (searchController.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).lowercased()

        // Show message instead of results if query is empty
        guard !newQuery.isEmpty else {
            currentSearch = nil
            tableView.reloadData()
            return
        }

        if let cachedSearch = searches[newQuery] {
            currentSearch = cachedSearch
            loadResults()
            tableView.reloadData()
        } else {
            let newSearch = GitHubRepositorySearch(query: newQuery)
            currentSearch = newSearch
            searches[newQuery] = newSearch

            // Delay the actual starting of the search by 500ms in case the user is still typing and to reduce load
            // on the API.
            startSearchTimer?.invalidate()
            startSearchTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                self?.loadResults()
            }
        }
    }
}
